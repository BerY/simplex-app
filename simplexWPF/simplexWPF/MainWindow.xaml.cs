﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace simplexWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int NumberofVariables;
        int NumberofLimitations;
        ComboBox MAXMIN;
        TextBox[] targetFunctionVariablesTextBox;
        TextBox[] restrictionTextBox;
        TextBox[,] variablesTextBox;
        public MainWindow()
        {
            InitializeComponent();
            CalculateButton.IsEnabled = false;
        }

        private void ClearButtonClick(object sender, RoutedEventArgs e)
        {
            NumLim.Clear();
            NumVar.Clear();
            OKButton.Visibility = Visibility.Visible;
            CalculateButton.IsEnabled = false;
            OpenFileButton.IsEnabled = true;
            FunctionStackPanel.Children.Clear();
            MatrixStackPanel.Children.Clear();
        }

        private void OkbuttonClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(NumVar.Text) || string.IsNullOrWhiteSpace(NumLim.Text))
            {
                MessageBox.Show("Text box can't be empty");
                return;
            }
            if (int.TryParse(NumVar.Text, out NumberofVariables) && int.TryParse(NumLim.Text, out NumberofLimitations))
            {
                OKButton.Visibility = Visibility.Collapsed;
                CalculateButton.IsEnabled = true;
                GenerateTargetFunctionGrid(NumberofVariables);
                GenerateMatrixGrid(NumberofVariables, NumberofLimitations);
            }
            else
            {
                MessageBox.Show("It is not a number!");
                return;
            }
        }

        private void CalculateButtonClick(object sender, RoutedEventArgs e)
        {
            double[] targetfunction = new double[NumberofVariables];
            double[,] variablesMatrix = new double[NumberofLimitations, NumberofVariables];
            double[] restriction = new double[NumberofLimitations];
            for(int i = 0; i < NumberofVariables; i++)
            {
                if (String.IsNullOrEmpty(targetFunctionVariablesTextBox[i].Text))
                {
                    MessageBox.Show("Fill all variables");
                    return;
                }
                targetfunction[i] = Convert.ToDouble(targetFunctionVariablesTextBox[i].Text);
            }
            for (int i = 0; i < NumberofLimitations; i++)
            {
                if (String.IsNullOrEmpty(restrictionTextBox[i].Text))
                {
                    MessageBox.Show("Fill all variables");
                    return;
                }
                restriction[i] = Convert.ToDouble(restrictionTextBox[i].Text);
                for (int j = 0; j < NumberofVariables; j++)
                {
                    if (String.IsNullOrEmpty(variablesTextBox[i, j].Text))
                    {
                        MessageBox.Show("Fill all variables");
                        return;
                    }
                    variablesMatrix[i, j] = Convert.ToDouble(variablesTextBox[i, j].Text);
                }
            }
            bool isMax;
            if (MAXMIN.Text == "MAX")
                isMax = true;
            else
                isMax = false;
            
            var s = new Simplex(targetfunction, variablesMatrix, restriction,isMax);
            s.maximize();
        }
        private void GenerateMatrixGrid(int col,int row)
        {
            restrictionTextBox = new TextBox[row];
            variablesTextBox = new TextBox[row,col];
            Label restrictions = new Label();
            restrictions.Content = "Restrictions:";
            StackPanel[] matrixRow = new StackPanel[row];
            MatrixStackPanel.Children.Add(restrictions);
            for(int i = 0; i < row; i++)
            {
                string mark = " + ";
                matrixRow[i] = new StackPanel();
                matrixRow[i].Orientation = Orientation.Horizontal;
                Label RowNumber = new Label();
                RowNumber.Content = "(" + (i+1) + ")";
                matrixRow[i].Children.Add(RowNumber);
                Label[] Xi = new Label[col];
                for(int j = 0; j < col;j++)
                {
                    if (col - 1 == j)
                        mark = " <= ";
                    Xi[j] = new Label();
                    Xi[j].Content = "X" + j.ToString() + mark;
                    variablesTextBox[i,j] = new TextBox();
                    variablesTextBox[i,j].Width = 30;
                    matrixRow[i].Children.Add(variablesTextBox[i,j]);
                    matrixRow[i].Children.Add(Xi[j]);
                }
                restrictionTextBox[i] = new TextBox();
                restrictionTextBox[i].Width = 30;
                matrixRow[i].Children.Add(restrictionTextBox[i]);
                MatrixStackPanel.Children.Add(matrixRow[i]);
            }
        }
        private void GenerateTargetFunctionGrid(int col)
        {
            MAXMIN = new ComboBox();
            MAXMIN.Items.Add("MAX");
            MAXMIN.Items.Add("MIN");
            MAXMIN.SelectedIndex = 0;
            MAXMIN.Width = 50;
            string mark = " + ";
            targetFunctionVariablesTextBox = new TextBox[col];
            Label Z = new Label();
            Label[] Xi = new Label[col];
            Z.Content = "Z = ";
            FunctionStackPanel.Children.Add(Z);
            for(int i = 0;i<col;i++)
            {
                if (col - 1 == i)
                    mark = " -> ";
                Xi[i] = new Label();
                Xi[i].Content = "X" + i.ToString() + mark;
                targetFunctionVariablesTextBox[i] = new TextBox();
                targetFunctionVariablesTextBox[i].Width = 30;
                FunctionStackPanel.Children.Add(targetFunctionVariablesTextBox[i]);
                FunctionStackPanel.Children.Add(Xi[i]);
            }
            FunctionStackPanel.Children.Add(MAXMIN);
        }

        private void OpenFileClick(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string file = dlg.FileName;
                using (TextReader reader = File.OpenText(file))
                {
                    try
                    {
                        string z = reader.ReadLine();
                        string[] var = z.Split(' ');
                        NumVar.Text = var.Length.ToString();
                        z = reader.ReadLine();
                        string[] lim = z.Split(' ');
                        NumLim.Text = lim.Length.ToString();
                        OkbuttonClick(null, e);
                        for(int i =0; i < NumberofVariables;i++)
                        {
                            targetFunctionVariablesTextBox[i].Text = var[i];
                        }
                        for(int i =0; i < NumberofLimitations;i++)
                        {
                            restrictionTextBox[i].Text = lim[i];
                        }
                        for (int i = 0; i < NumberofLimitations; i++)
                        {
                            z = reader.ReadLine();
                            string[] tmp = z.Split(' ');
                            for (int j = 0; j < NumberofVariables; j++)
                            {
                                variablesTextBox[i, j].Text = tmp[j];
                            }
                        }
                    }
                    catch(Exception)
                    {
                        MessageBox.Show("Wrong data format or spaces in end of lines!");
                        ClearButtonClick(null, e);
                        return;
                    }
                }
            }
            OpenFileButton.IsEnabled = false;
        }
    }
}
