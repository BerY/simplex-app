﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Data;

namespace simplexWPF
{
    class Simplex
    {

        int J;
        int I;

        double[] cj;
        double[,] A;
        double[] bi;

        double _result;
        double[] ci;
        double[] zj;
        double[] cjzj;
        double[] bA;
        int[] zb;
        int maxIndex = -1;
        int minIndex = -1;
        TableResults win2 = new TableResults();

        public void print()
        {
            DataGrid resultDataGrid = new DataGrid();
            resultDataGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            DataTable resultDataTable = new DataTable();

            resultDataTable.Columns.Add("Cb", typeof(string));
            resultDataTable.Columns.Add("Xb", typeof(string));
            for (int j = 0; j < J; j++)
            {
                resultDataTable.Columns.Add("X" + (j+1).ToString(), typeof(string));
            }
            resultDataTable.Rows.Add();
            resultDataTable.Columns.Add("B", typeof(string));
            for (int i = 0, j = 0; i < I; i++)
            {
                resultDataTable.Rows.Add();
                resultDataTable.Rows[i][j] = ci[i];
                resultDataTable.Rows[i][j+1] = "X" + zb[i].ToString();
                for (j = 1; j <= J; j++)
                    resultDataTable.Rows[i][j+1] = A[i,j-1];
                resultDataTable.Rows[i][j+1] = bi[i];
                j = 0;
            }
            resultDataTable.Rows.Add();
            resultDataTable.Rows[I][1] = "Zj";
            resultDataTable.Rows[I+1][1] = "Cj-Zj";
            for (int i = 0; i < J;i++)
            {
                resultDataTable.Rows[I][i+2] = zj[i];
                resultDataTable.Rows[I + 1][i + 2] = cjzj[i];
            }
            resultDataGrid.ItemsSource = resultDataTable.DefaultView;
            win2.ResultStackPanel.Children.Add(resultDataGrid);
        }

        private void printResult()
        {
            Label resultLabel = new Label();
            resultLabel.Content = "The optimal solution is: " + _result.ToString();
            win2.ResultStackPanel.Children.Add(resultLabel);
            Label[] cjzjLabel = new Label[J];
            for (int j = 0; j < I; j++)
            {
                cjzjLabel[j] = new Label();
                cjzjLabel[j].Content = "X" + zb[j].ToString() + " = " + bi[j].ToString(); 
                win2.ResultStackPanel.Children.Add(cjzjLabel[j]);
            }
            win2.Show();
        }

        public Simplex(double[] f, double[,] a, double[] b, bool max = true)
        {
            int NUMOF_VARS = f.Length;
            int NUMOF_LIMITS = a.GetLength(0);

            this.J = NUMOF_VARS + NUMOF_LIMITS;
            this.I = NUMOF_LIMITS;

            // Komorki wejsciowe
            this.cj = new double[this.J];
            this.A = new double[this.I, this.J];
            this.bi = new double[this.I];


            // Komorki pomocnicze
            this._result = 0.0;

            this.ci = new double[this.I];
            this.zj = new double[this.J];
            this.cjzj = new double[this.J];
            this.bA = new double[this.I];
            this.zb = new int[this.I];


            for (int x = 0; x < NUMOF_VARS; x++)
                cj[x] = f[x];

            for (int x = 0; x < I; x++)
                for (int y = 0; y < NUMOF_VARS ; y++)
                    A[x, y] = a[x, y];

            for (int x = 0; x < I; x++)
                for (int y = NUMOF_VARS; y < J; y++)
                    A[x, y] = (x == y - NUMOF_LIMITS) ? 1.0 : 0.0;

            int changeMaxMin = (max) ? 1 : -1;

            for (int x = 0; x < NUMOF_VARS; x++)
                cj[x] = changeMaxMin * f[x];

            for (int i = 0; i < I; i++)
                bi[i] = b[i];

            for (int i = 0; i < I; i++)
            {
                zb[i] = I + i + 1;
            }
        }

        public void calculateZJ()
        {
            for (int j = 0; j < J; j++)
            {
                double partialRes = 0.0;

                for (int i = 0; i < I; i++)
                    partialRes += ci[i] * A[i, j];

                zj[j] = partialRes;
            }
        }

        public void calculateCJZJ()
        {
            for (int j = 0; j < J; j++)
            {
                cjzj[j] = cj[j] - zj[j];
            }
        }

        public void updateMaxIdxCJZJ()
        {
            maxIndex = cjzj.ToList().IndexOf(cjzj.Max());
        }

        public void calculatebA()
        {
            updateMaxIdxCJZJ();

            int j = maxIndex;

            for (int i = 0; i < I; i++)
            {
                bA[i] = bi[i] / A[i, j];
            }
        }


        public void updateMinIdxbA()
        {
            minIndex = bA.ToList().IndexOf(bA.Min());
        }

        public void swapZB()
        {
            zb[minIndex] = maxIndex + 1;
        }

        public void updateCI()
        {
            ci[minIndex] = cj[maxIndex];
        }

        public void calculateNewA()
        {
            int pivotElementI = minIndex;
            int pivodElementJ = maxIndex;

            double pivodElememtValue = A[pivotElementI, pivodElementJ];
            // update pvot throw

            for (int j = 0; j < J; j++)
                A[pivotElementI, j] = 1.0 / pivodElememtValue * A[pivotElementI, j];
            bi[pivotElementI] = 1.0 / pivodElememtValue * bi[pivotElementI];

            for (int i = 0; i < I; i++)
            {
                if (i == pivotElementI)
                    continue;

                pivodElememtValue = A[i, pivodElementJ];
                for (int j = 0; j < J; j++)
                {
                    A[i, j] = A[i, j] - pivodElememtValue * A[pivotElementI, j];
                }
                bi[i] = bi[i] - pivodElememtValue * bi[pivotElementI];
            }
        }

        public void getResult()
        {
            double res = 0.0;

            for (int i = 0; i < I; i++)
                res += ci[i] * bi[i];

            _result = res;
        }

        public void maximize()
        {
            calculateZJ();
            calculateCJZJ();

            while (!cjzj.All(element => element <= 0))
            {
                calculatebA();
                updateMinIdxbA();
                swapZB();
                updateCI();
                calculateNewA();
                calculateZJ();
                calculateCJZJ();
                getResult();
                print();
            }
            printResult();
           
        }

    }
}
