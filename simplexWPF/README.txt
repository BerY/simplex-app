Simplex

Bernard Rusin & Kamil Pis

OPIS:

Program sluzy do rozwiazywania zagadnien linowych.

OBSLUGA:

Po uruchomieniu programu konieczne jest podanie ilosci zmiennych w modelu.
Natepnym krokiem jest uzupelnienie rownan lub wczytanie z pliku.

Format PLIKU (Nie moze byc spacji na koncu lini):


1 3 2    # wektor wspolczynnikow funkcji celu
5 4 1	 # wektor wspolczynnikow b 
1 2 1	 # macierz A
1 1 1
0 1 2

Przykladowe pliki w dolaczone w katalogu

Oznazcenia na tabelach smiplex:

cb - wsp�czynniki przy zmiennych bazowych w funkcji celu.
cj - wsp�czynniki przy zmiennych w funkcji celu.
bi - warto�ci zmiennych bazowych.
zj - wska�niki pomocnicze.
cj - zj - wska�niki optymalno�ci (kryterium simpleks).
zb - zmienne bazowe

ZNANE OGRANICZENIA:

* Program nie obsuguje = w warunkach (tylko <=)
* Program nie wykrywa ukladow sprzecznych, nieograczonych lub alternatywch